package com.myspring.admin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@MapperScan("com.myspring.admin.mapper")
@SpringBootApplication
public class ElAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElAdminApplication.class, args);
    }

}
