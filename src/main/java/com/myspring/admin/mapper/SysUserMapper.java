package com.myspring.admin.mapper;

import com.myspring.admin.BaseEntity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【sys_user(系统用户)】的数据库操作Mapper
* @createDate 2024-02-02 17:20:03
* @Entity com.myspring.admin.BaseEntity.SysUser
*/
public interface SysUserMapper extends BaseMapper<SysUser> {

}




