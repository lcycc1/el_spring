package com.myspring.admin.service;

import com.myspring.admin.BaseEntity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【sys_user(系统用户)】的数据库操作Service
* @createDate 2024-02-02 17:20:03
*/
public interface SysUserService extends IService<SysUser> {

}
